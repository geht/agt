#pragma once

struct PathTracer
{
	static const int NUM_SPHERE_SAMPLES = MONTE_CARLO_SAMPLES * MONTE_CARLO_SAMPLES;

	Scene scene;
	ScreenPlane screenPlane;

	glm::vec3 * renderSerial()
	{
		glm::vec3 * image = new glm::vec3[SCREEN_WIDTH * SCREEN_HEIGHT];
		glm::vec3 origin(0.0f, 0.0f, -5.0f);

		for (int y = 0; y < SCREEN_HEIGHT; y++)
		{
			for (int x = 0; x < SCREEN_WIDTH; x++)
			{
				glm::vec3 accumulatedColor(0.0f, 0.0f, 0.0f);

				// spawn SUPER_SAMPLES * SUPER_SAMPLES primary rays per pixel for supersampling/anti-aliasing
				for (int ty = 0; ty < SUPER_SAMPLES; ty++)
				{
					for (int tx = 0; tx < SUPER_SAMPLES; tx++)
					{
						Ray ray = screenPlane.constructRay(origin, x, y, tx, ty, SUPER_SAMPLES);
						float distance;
						Primitive * foundSphere = trace(ray, accumulatedColor, 1, 1.0f, distance);
					}
				}

				image[SCREEN_WIDTH * y + x] = accumulatedColor / float(SUPER_SAMPLES * SUPER_SAMPLES);
			}
		}

		return image;
	}

	glm::vec3 * renderParallel()
	{
		glm::vec3 * image = new glm::vec3[SCREEN_WIDTH * SCREEN_HEIGHT];
		glm::vec3 origin(0.0f, 0.0f, -5.0f);

		tbb::parallel_for(tbb::blocked_range2d<int>(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT), [&](const tbb::blocked_range2d<int> & r)
		{
			for (int y = r.cols().begin(), y_end = r.cols().end(); y < y_end; y++)
			{
				for (int x = r.rows().begin(), x_end = r.rows().end(); x < x_end; x++)
				{
					glm::vec3 accumulatedColor(0.0f, 0.0f, 0.0f);

					// spawn SUPER_SAMPLES * SUPER_SAMPLES primary rays per pixel for supersampling/anti-aliasing
					for (int ty = 0; ty < SUPER_SAMPLES; ty++)
					{
						for (int tx = 0; tx < SUPER_SAMPLES; tx++)
						{
							Ray ray = screenPlane.constructRay(origin, x, y, tx, ty, SUPER_SAMPLES);
							float distance;
							Primitive * foundSphere = trace(ray, accumulatedColor, 1, 1.0f, distance);
						}
					}

					image[SCREEN_WIDTH * y + x] = accumulatedColor / float(SUPER_SAMPLES * SUPER_SAMPLES);
				}
			}
		});

		return image;
	}

	Primitive * trace(Ray & ray, glm::vec3 & accumulatedColor, int depth, float refractionIndex, float & distance)
	{
		// return if over maximum trace depth
		if (depth > MAX_TRACE_DEPTH) return 0;

		distance = std::numeric_limits<float>::max();
		glm::vec3 pointOfIntersection;
		Primitive * hitPrimitive = 0;
		int intersectionResult; // 0 = miss, 1 = hit, -1 = hit from the inside of the primitive

		// find the nearest intersection
		// go over every object, check if there is an intersection, keep the nearest one
		for (int o = 0; o < scene.numPrimitives; o++)
		{
			Primitive * primitive = scene.primitives[o];
			int hit = primitive->intersect(ray, distance);

			if (hit)
			{
				hitPrimitive = primitive;
				intersectionResult = hit;
			}
		}

		// if there is no hit, terminate ray
		if (!hitPrimitive) return 0;

		// else handle the intersection
		// determine color at intersection point
		if (hitPrimitive->isLight)
		{
			// accumulatedColor += hitPrimitive->material.color;
			// accumulatedColor = glm::vec3(1.0f, 1.0f, 1.0f); // return white light when looking directly at the light
			accumulatedColor += hitPrimitive->material.color;
			return hitPrimitive;
		}

		// direct illumination
		// paths of length 1
		if (!USE_LIGHTSOURCE_SAMPLING)
		{
			accumulatedColor += calculateSimpleDirectIlluminationHemisphereSampling(hitPrimitive, ray, depth, refractionIndex, distance);
		}
		else
		{
			accumulatedColor += calculateDirectIlluminationLightsourceSampling(hitPrimitive, ray, depth, refractionIndex, distance);
		}

		// indirect illumination
		// TODO: implement indirect illumination

		// return pointer to primitive hit by primary ray
		return hitPrimitive;
	}

	// random walks
	// sample the hemisphere at every intersection point
	// send rays in all directions
	// has a high variance: most of the paths will not end at the light source
	// NOTE: this takes NUM_SPHERE_SAMPLES samples (per given intersection point)
	glm::vec3 calculateSimpleDirectIlluminationHemisphereSampling(Primitive * hitPrimitive, Ray & ray, int depth, float refractionIndex, float & distance)
	{
		// NOTE: this is only diffuse for now
		// TODO: implement specular
		if (hitPrimitive == NULL || hitPrimitive->material.diffuse <= 0.0f) return glm::vec3(0.0f);
		int n = 0;
		glm::vec3 pointOfIntersection = ray.evaluate(distance);
		glm::vec3 surfaceNormal = hitPrimitive->normal(pointOfIntersection, ray.direction);

		glm::vec3 q = glm::vec3(0.0f, 0.0f, 0.0f);
		glm::vec3 samples[NUM_SPHERE_SAMPLES];
		generateSphericalSamples(samples, MONTE_CARLO_SAMPLES);
		for (int s = 0; s < NUM_SPHERE_SAMPLES; s++)
		{
			glm::vec3 d = samples[s];
			float dot = glm::dot(d, surfaceNormal);
			// we only want vectors around the hemisphere
			// so we turn those on the wrong hemisphere around
			if (dot < 0.0f)
			{
				d = -d;
				dot = -dot;
			}

			// construct a new ray
			Ray sampleRay = { pointOfIntersection + d * EPSILON, d };

			// find the nearest intersection with a light source
			// go over every light, check if there is an intersection, keep the nearest one
			Primitive * lightSource = 0;
			for (int o = 0; o < scene.numPrimitives; o++)
			{
				Primitive * primitive = scene.primitives[o];
				if (!primitive->isLight) continue;
				if (primitive->intersect(sampleRay, distance))
				{
					lightSource = primitive;
				}
			}
			if (lightSource == NULL) continue;

			q += hitPrimitive->material.color * hitPrimitive->material.diffuse * dot * lightSource->material.color;
			n++;
		}
		q /= n;
		q /= glm::pi<float>();

		return q;
	}

	// random walks
	// place samples directly on the light source, not on the hemisphere
	// all rays that are generated this way will contribute to the final radiance value
	// NOTE: this takes NUM_SPHERE_SAMPLES samples PER light source (per given intersection point)
	glm::vec3 calculateDirectIlluminationLightsourceSampling(Primitive * hitPrimitive, Ray & ray, int depth, float refractionIndex, float & distance)
	{
		if (hitPrimitive == NULL) return glm::vec3(0.0f);
		glm::vec3 q = glm::vec3(0.0f, 0.0f, 0.0f);

		int n = 0;
		float shininess = 20.0f;
		float sfactor = (shininess + 2.0f) / (2.0f * glm::pi<float>());
		glm::vec3 pointOfIntersection = ray.evaluate(distance);
		glm::vec3 N = hitPrimitive->normal(pointOfIntersection, ray.direction);

		// place samples on each light source
		// trace lights
		for (int o = 0; o < scene.numPrimitives; o++)
		{
			// this search for lights is expensive...
			// TODO: seperate light sources from the other primitives
			// keep them in some special collection
			Primitive * l = scene.primitives[o];
			if (!l->isLight) continue;

			// handle point light source
			// TODO: implement other kinds of light sources
			if (l->getType() == Primitive::SPHERE)
			{
				glm::vec3 samples[NUM_SPHERE_SAMPLES];
				generateSphericalSamples(samples, MONTE_CARLO_SAMPLES);
				for (int s = 0; s < NUM_SPHERE_SAMPLES; s++, n++)
				{
					glm::vec3 samplePointOnLight = ((Sphere*)l)->center - ((Sphere*)l)->radius * samples[s];
					glm::vec3 L = samplePointOnLight - pointOfIntersection;
					float dist = glm::length(L);
					L *= 1.0f / dist;

					// spawn a shadow ray
					// determine visibility of the light source
					float visibility = 1.0f;
					Ray shadowRay = { pointOfIntersection + L * EPSILON, L };
					for (int s = 0; s < scene.numPrimitives; s++)
					{
						Primitive * pr = scene.primitives[s];
						if (!pr->isLight && pr->intersect(shadowRay, dist))
						{
							visibility = 0.0f; // there is something in the way
							break; // we can break here, no need to find the nearest object that blocks the view
						}
					}

					// calculate diffuse shading
					// lambertian reflectance
					if (hitPrimitive->material.diffuse > 0.0f)
					{
						float dot = glm::dot(N, L);
						if (dot > 0.0f)
						{
							float diffuse = dot * hitPrimitive->material.diffuse * visibility;
							// add diffuse component to accumulated color
							q += hitPrimitive->material.color * diffuse * l->material.color; // / glm::pi<float>();
						}
					}
					
					// calculate specular component
					// phong model
					if (hitPrimitive->material.specular > 0.0f)
					{
						// TODO: use blinn-phong model
						glm::vec3 V = ray.direction;
						glm::vec3 R = L - 2.0f * glm::dot(L, N) * N;

						float dot = glm::dot(V, R);

						if (dot > 0.0f)
						{
							float specular = glm::pow(dot, shininess) * hitPrimitive->material.specular * visibility;
							// add specular component to accumulated color
							q += specular * l->material.color * sfactor;
						}
					}
				}
			}
		}
		q /= n;

		return q;
	}

	// generates a random sample uniformly distributed over the sphere (not the hemisphere)
	static glm::vec3 generateSphericalSample(float a, float b)
	{
		// generate a uniformly distributed sample on the sphere in polar coordinates
		float theta = 2.0f * glm::acos(glm::sqrt(1.0f - a));
		float phi = 2.0f * glm::pi<float>() * b;

		// convert them to cartesian coordinates (radius = 1)
		float x = glm::sin(theta) * glm::cos(phi);
		float y = glm::sin(theta) * glm::sin(phi);
		float z = glm::cos(theta);

		return glm::vec3(x, y, z);
	}

	// fills samples with numSamples * numSamples random samples uniformly distributed over the sphere (not the hemisphere)
	static void generateSphericalSamples(glm::vec3 * samples, int numSamples)
	{
		float w = 1.0f / numSamples;

		int s = 0;
		for (int i = 0; i < numSamples; i++)
		{
			for (int j = 0; j < numSamples; j++)
			{
				// generate two uniform random variables a and b within the interval [0, 1]
				float a = glm::linearRand(0.0f, 1.0f);
				float b = glm::linearRand(0.0f, 1.0f);

				if (USE_STRATIFIED_SAMPLING)
				{
					a = a * w + i * w;
					b = b * w + j * w;
				}

				samples[s] = generateSphericalSample(a, b);
				s++;
			}
		}
	}
};