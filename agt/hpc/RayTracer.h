#pragma once

struct RayTracer
{
	Scene scene;
	ScreenPlane screenPlane;

	glm::vec3 * renderSerial()
	{
		glm::vec3 * image = new glm::vec3[SCREEN_WIDTH * SCREEN_HEIGHT];
		glm::vec3 origin(0.0f, 0.0f, -5.0f);

		for (int y = 0; y < SCREEN_HEIGHT; y++)
		{
			for (int x = 0; x < SCREEN_WIDTH; x++)
			{
				glm::vec3 accumulatedColor(0.0f, 0.0f, 0.0f);

				// spawn SUPER_SAMPLES * SUPER_SAMPLES primary rays per pixel for supersampling/anti-aliasing
				for (int ty = 0; ty < SUPER_SAMPLES; ty++)
				{
					for (int tx = 0; tx < SUPER_SAMPLES; tx++)
					{
						Ray ray = screenPlane.constructRay(origin, x, y, tx, ty, SUPER_SAMPLES);
						float distance;
						Primitive * foundSphere = trace(ray, accumulatedColor, 1, 1.0f, distance);
					}
				}

				image[SCREEN_WIDTH * y + x] = accumulatedColor / float(SUPER_SAMPLES * SUPER_SAMPLES);
			}
		}

		return image;
	}

	glm::vec3 * renderParallel()
	{
		glm::vec3 * image = new glm::vec3[SCREEN_WIDTH * SCREEN_HEIGHT];
		glm::vec3 origin(0.0f, 0.0f, -5.0f);

		tbb::parallel_for(tbb::blocked_range2d<int>(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT), [&](const tbb::blocked_range2d<int> & r)
		{
			for (int y = r.cols().begin(), y_end = r.cols().end(); y < y_end; y++)
			{
				for (int x = r.rows().begin(), x_end = r.rows().end(); x < x_end; x++)
				{
					glm::vec3 accumulatedColor(0.0f, 0.0f, 0.0f);

					// spawn SUPER_SAMPLES * SUPER_SAMPLES primary rays per pixel for supersampling/anti-aliasing
					for (int ty = 0; ty < SUPER_SAMPLES; ty++)
					{
						for (int tx = 0; tx < SUPER_SAMPLES; tx++)
						{
							Ray ray = screenPlane.constructRay(origin, x, y, tx, ty, SUPER_SAMPLES);
							float distance;
							Primitive * foundSphere = trace(ray, accumulatedColor, 1, 1.0f, distance);
						}
					}

					image[SCREEN_WIDTH * y + x] = accumulatedColor / float(SUPER_SAMPLES * SUPER_SAMPLES);
				}
			}
		});

		return image;
	}

	glm::vec3 * renderParallel_ParallelSS()
	{
		glm::vec3 * image = new glm::vec3[SCREEN_WIDTH * SCREEN_HEIGHT];
		glm::vec3 origin(0.0f, 0.0f, -5.0f);

		tbb::parallel_for(tbb::blocked_range2d<int>(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT), [&](const tbb::blocked_range2d<int> & r)
		{
			for (int y = r.cols().begin(), y_end = r.cols().end(); y < y_end; y++)
			{
				for (int x = r.rows().begin(), x_end = r.rows().end(); x < x_end; x++)
				{
					glm::vec3 accumulatedColor(0.0f, 0.0f, 0.0f);

					// spawn SUPER_SAMPLES * SUPER_SAMPLES primary rays per pixel for supersampling/anti-aliasing
					tbb::parallel_for(tbb::blocked_range2d<int>(0, SUPER_SAMPLES, 0, SUPER_SAMPLES), [&](const tbb::blocked_range2d<int> & s)
					{
						for (int ty = s.cols().begin(), ty_end = s.cols().end(); ty < ty_end; ty++)
						{
							for (int tx = s.rows().begin(), tx_end = s.rows().end(); tx < tx_end; tx++)
							{
								Ray ray = screenPlane.constructRay(origin, x, y, tx, ty, SUPER_SAMPLES);
								float distance;
								Primitive * foundSphere = trace(ray, accumulatedColor, 1, 1.0f, distance);
							}
						}
					});

					image[SCREEN_WIDTH * y + x] = accumulatedColor / float(SUPER_SAMPLES * SUPER_SAMPLES);
				}
			}
		});

		return image;
	}

	Primitive * trace(Ray & ray, glm::vec3 & accumulatedColor, int depth, float refractionIndex, float & distance)
	{
		// return if over maximum trace depth
		if (depth > MAX_TRACE_DEPTH) return 0;

		distance = std::numeric_limits<float>::max();
		glm::vec3 pointOfIntersection;
		Primitive * hitPrimitive = 0;
		int intersectionResult; // 0 = miss, 1 = hit, -1 = hit from the inside of the primitive

		// find the nearest intersection
		// go over every object, check if there is an intersection, keep the nearest one
		for (int o = 0; o < scene.numPrimitives; o++)
		{
			Primitive * primitive = scene.primitives[o];
			int hit = primitive->intersect(ray, distance);

			if (hit)
			{
				hitPrimitive = primitive;
				intersectionResult = hit;
			}
		}

		// if there is no hit, terminate ray
		if (!hitPrimitive) return 0;

		// else handle the intersection
		// determine color at intersection point
		if (hitPrimitive->isLight)
		{
			accumulatedColor += hitPrimitive->material.color;
			// accumulatedColor = glm::vec3(1.0f, 1.0f, 1.0f); // return white light when looking directly at the light
			return hitPrimitive;
		}

		pointOfIntersection = ray.evaluate(distance);
		// trace lights
		for (int o = 0; o < scene.numPrimitives; o++)
		{
			Primitive * l = scene.primitives[o];
			if (!l->isLight) continue;

			// handle point light source
			// determine whether the point of intersection can be seen by the light source (or the other way around)
			float shade = 1.0f;
			if (l->getType() == Primitive::SPHERE)
			{
				glm::vec3 L = ((Sphere*)l)->center - pointOfIntersection;
				float dist = glm::length(L);
				L *= 1.0f / dist;

				// spawn a secondary ray
				Ray secondaryRay = { pointOfIntersection + L * EPSILON, L };
				for (int s = 0; s < scene.numPrimitives; s++)
				{
					Primitive * pr = scene.primitives[s];
					if (!pr->isLight && pr->intersect(secondaryRay, dist))
					{
						shade = 0.0f; // there is something in the way
						break; // we can break here, no need to find the nearest object that blocks the view
					}
				}
			}

			// calculate diffuse shading
			glm::vec3 L = ((Sphere*)l)->center - pointOfIntersection;
			L = glm::normalize(L);

			glm::vec3 N = hitPrimitive->normal(pointOfIntersection, ray.direction);
			if (hitPrimitive->material.diffuse > 0.0f)
			{
				float dot = glm::dot(N, L);
				if (dot > 0)
				{
					float diffuse = dot * hitPrimitive->material.diffuse * shade;
					// add diffuse component to accumulated color
					accumulatedColor += diffuse * hitPrimitive->material.color * l->material.color / glm::pi<float>();
				}
			}

			// calculate specular component
			if (hitPrimitive->material.specular > 0)
			{
				glm::vec3 V = ray.direction;
				glm::vec3 R = L - 2.0f * glm::dot(L, N) * N;
				float dot = glm::dot(V, R);
				if (dot > 0)
				{
					float specular = powf(dot, 20) * hitPrimitive->material.specular * shade;

					// add specular component to accumulated color
					accumulatedColor += specular * l->material.color;
				}
			}
		}

		// calculate reflection
		float reflection = hitPrimitive->material.reflection;
		if (reflection > 0.0f)
		{
			glm::vec3 N = hitPrimitive->normal(pointOfIntersection, ray.direction);
			glm::vec3 R = ray.direction - 2.0f * glm::dot(ray.direction, N) * N;
			if (depth < MAX_TRACE_DEPTH)
			{
				glm::vec3 reflColor = glm::vec3(0.0f, 0.0f, 0.0f);
				float dist;

				// spawn a secondary ray
				Ray secondaryRay = { pointOfIntersection + R * EPSILON, R };
				trace(secondaryRay, reflColor, depth + 1, refractionIndex, dist);

				// add reflection component to accumulated color
				accumulatedColor += reflection * reflColor * hitPrimitive->material.color;
			}
		}

		// calculate refraction using Snell's law
		float refraction = hitPrimitive->material.refraction;
		if (refraction > 0.0f && depth < MAX_TRACE_DEPTH)
		{
			float refrIndex = hitPrimitive->material.refractionIndex;
			float n = refractionIndex / refrIndex;
			glm::vec3 N = hitPrimitive->normal(pointOfIntersection, ray.direction) * (float)intersectionResult; // flips the normal if the hit was from the inside
			float cosI = -glm::dot(N, ray.direction);
			float cosT2 = 1.0f - n * n * (1.0f - cosI * cosI);

			if (cosT2 > 0.0f)
			{
				glm::vec3 T = (n * ray.direction) + (n * cosI - glm::sqrt(cosT2)) * N;
				glm::vec3 refrColor = glm::vec3(0.0f, 0.0f, 0.0f);
				float dist;

				// spawn a secondary ray
				Ray secondaryRay = { pointOfIntersection + T * EPSILON, T };
				trace(secondaryRay, refrColor, depth + 1, refrIndex, dist);

				// apply Beer's law (light falls off in the material, a part of the light gets absorbed by the material)
				glm::vec3 absorbance = hitPrimitive->material.color * 0.15f * -dist;
				glm::vec3 transparency = glm::vec3(glm::exp(absorbance.r), glm::exp(absorbance.g), glm::exp(absorbance.b));

				// add refraction component to accumulated color
				accumulatedColor += refrColor * transparency;
			}
		}

		// return pointer to primitive hit by primary ray
		return hitPrimitive;
	}
};